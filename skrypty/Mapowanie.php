<?php
require_once 'skrypty/Ksiazka.php';

class Mapowanie {
    public function mapuj($tablicaKsiazek) {
        //var_dump($tablicaKsiazek);
        $tablicaObiektow = array ();
        foreach ($tablicaKsiazek as $daneKsiazki){
            //var_dump($daneKsiazki);
          $danePociete = explode(";", $daneKsiazki);
          //var_dump($danePociete);
          $ksiazka = new Ksiazka();
          for ($i = 0; $i < count($i); $i++){
              $ksiazka->setId($danePociete[0]);
              $ksiazka->setTytul($danePociete[1]);
              $ksiazka->setAutor($danePociete[2]);
              $ksiazka->setKategoria($danePociete[3]);
              $ksiazka->setData_wydania($danePociete[4]);
              $tablicaObiektow[] = $ksiazka;
          }
        }
        
        return $tablicaObiektow;
    }
}
