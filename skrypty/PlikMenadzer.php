<?php

class PlikMenadzer {
    
    const SCIEZKA_PLIKU = "pliki/biblioteka.txt";
    
    private $uchwytDoPliku;
    
    public function otworz() {
    $this->uchwytDoPliku = fopen(self::SCIEZKA_PLIKU, 'a+');
    }  
    private function otworzISkasuj()
    {
        $this->uchwytDoPliku = fopen(self::SCIEZKA_PLIKU, 'w+');
    }
    public function usun($ksiazka)
    {        
        $zawartosc = $this->zwrocZawartosc();
        $ksiazkiTablica = '';
        foreach ($zawartosc as $pozycja) {

            if ( !($ksiazka->getId() == explode(";",$pozycja)[0]))
            {
                $ksiazkiTablica .= $pozycja;
            } 
        }
        $this->wyczyscPlik();
        fwrite($this->uchwytDoPliku, $ksiazkiTablica);
    }
    public function zwrocZawartosc() {
        $zawartoscPliku = file(self::SCIEZKA_PLIKU);
        return $zawartoscPliku;
    }
    
    public function edytuj($edytowanaKsiazka)
    {
        $zawartosc = $this->zwrocZawartosc();
        $tekst = '';
        foreach ($zawartosc as &$pozycja) {
            if ($edytowanaKsiazka[0] == $pozycja[0])
            {
               $pozycja = $edytowanaKsiazka . "\n";
            }
            $tekst .= $pozycja;
        }
        $this->wyczyscPlik();
        fwrite($this->uchwytDoPliku, $tekst);
    }
    
    public function dodaj($ksiazka)
    {   
        fwrite($this->uchwytDoPliku,  "\n" .
             $ksiazka->getId() . ";" . $ksiazka->getTytul() . ";"
            . $ksiazka->getAutor() . ";" . $ksiazka->getKategoria() . ";"
            . $ksiazka->getData_wydania() . ";"
        );
    }
    
    private function wyczyscPlik()
    {
        $this->zamknij();
        $this->otworzISkasuj();
    }
    
    public function zamknij() {
        $this->uchwytDoPliku = fclose($this->uchwytDoPliku);
    }
} 