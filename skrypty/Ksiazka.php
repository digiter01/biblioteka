<?php


class Ksiazka {
    private $id;
    private $tytul;
    private $autor;
    private $kategoria;
    private $data_wydania;
    
    function getId() {
        return $this->id;
    }

    function getTytul() {
        return $this->tytul;
    }

    function getAutor() {
        return $this->autor;
    }

    function getKategoria() {
        return $this->kategoria;
    }

    function getData_wydania() {
        return $this->data_wydania;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTytul($tytul) {
        $this->tytul = $tytul;
    }

    function setAutor($autor) {
        $this->autor = $autor;
    }

    function setKategoria($kategoria) {
        $this->kategoria = $kategoria;
    }

    function setData_wydania($data_wydania) {
        $this->data_wydania = $data_wydania;
    }


    
    
}
