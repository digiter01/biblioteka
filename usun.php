<?php

    require_once 'skrypty/PlikMenadzer.php';
    require_once 'skrypty/Mapowanie.php';
    require_once 'skrypty/Ksiazka.php';
    
    $menadzer = new PlikMenadzer();
    $menadzer->otworz();
      
    $zawartosc = $menadzer->zwrocZawartosc();

    $mapowanie = new Mapowanie();
    $tablicaObiektow = $mapowanie->mapuj($zawartosc);

    foreach ($tablicaObiektow as $ksiazka)
    {
      if($_GET['id'] == $ksiazka->getId()) 
        {
            $menadzer->usun($ksiazka);    
        }
    }
    
    header("location: index.php");
