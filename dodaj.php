<?php

require_once 'skrypty/Ksiazka.php';
require_once 'skrypty/PlikMenadzer.php';
if (!empty($_POST))
{
    $ksiazka = new Ksiazka();
    $ksiazka->setId($_POST['id']);
    $ksiazka->setTytul($_POST['tytul']);
    $ksiazka->setAutor($_POST['autor']);
    $ksiazka->setKategoria($_POST['kategoria']);
    $ksiazka->setData_wydania($_POST['data_wydania']);
    
    $menadzer = new PlikMenadzer();
    $menadzer->otworz();
    $menadzer->dodaj($ksiazka);
    $menadzer->zamknij();
    
    header("location: index.php");
}
require_once 'szablony/dodaj.php';
